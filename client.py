from requests import put, get
from subprocess import call, check_output
from collections import Counter
import socket
import time
import sys


USERNAME = ''
PASSWORD = ''

EXCEPTION_LIST = [
                    '192.168.1.', 
                    '97.93.23.125'
                   ]

LINES_TO_PARSE = 1000
BLOCK_AFTER_THIS_MANY = 5
BUSTED_THIS_SESSION = []


"""  This is where IPs that are detected as malicious are posted to the hive """
def call_home(ip):
    put('https://loudtree.net/baddies', data={'badguy': ip,
                                               'username': USERNAME,
                                               'password': PASSWORD
                                                }, verify=True).json()


""" This returns a huge dump of IPs known to be malicious and then bans them via your iptables - it is called at init """
def block_baddies():
    list_of_ips = []
    ips = get('https://loudtree.net/getbaddies',
        data={'username': USERNAME,
              'password': PASSWORD},
        verify=True
        ).json()

    #fix this on the back end so it can be easily handled
    for i in ips['Success'].split(','):
        i = i.replace('(','').replace(')', '').replace('\'','').replace('\n', '').replace(' ', '')
        if i != '':
            list_of_ips.append(str(i))
    for i in list_of_ips:
        block_bad_sources(i)
    return list_of_ips


def convert_dns_to_ip(source):
    source_ip = socket.gethostbyname(source)
    return source_ip


def block_bad_sources(targetsource):
    for exception in EXCEPTION_LIST:
        if exception not in targetsource:
            call(['iptables', '-D', 'INPUT', '-s', str(targetsource), '-j', 'DROP'])
            call(['iptables', '-A', 'INPUT', '-s', str(targetsource), '-j', 'DROP'])
            call(['iptables-save'])


def discover_bad_auths(FAILED_AUTHS, BLOCK_AFTER_THIS_MANY):
    print("Starting log monitor with:\nlength=%s\nthreshold=%s" % (BLOCK_AFTER_THIS_MANY, BLOCK_AFTER_THIS_MANY))
    baddies = []

    try:
        authlog = check_output(['tail', '--lines='+str(BLOCK_AFTER_THIS_MANY), '/var/log/auth.log']).split('\n')
        for entry in authlog:
            try:
                if "authentication failure" in entry and "rhost= " not in entry:
                    offending_source = entry.split("rhost=")[1].split(" ")[0]
                    source_ip = convert_dns_to_ip(offending_source)

                    print("Appending %s to Baddies Bucket - we've never seen it before" % (source_ip))
                    baddies.append(source_ip)

                if "Invalid user " in entry and " from " in entry:
                    offending_source = entry.split(" from ")[1].split(" ")[0]
                    source_ip = convert_dns_to_ip(offending_source)

                    print("Appending %s to Baddies Bucket - we've never seen it before" % (source_ip))
                    baddies.append(source_ip)

            except:
                pass

            try:
                if "Connection closed by " in entry and " [preauth]" in entry:
                    offending_source = entry.split("Connection closed by ")[1].split(" [preauth")[0]
                    source_ip = convert_dns_to_ip(offending_source)

                    print("Appending %s to Baddies Bucket - we've never seen it before" % (source_ip))
                    baddies.append(source_ip)
            except:
                pass
    except:
        print("/var/log/auth.log not found.  Skipping.")
        pass
    #new authlog (journaled)
    try:
        journal_authlog = check_output(["journalctl -u sshd -n", LINES_TO_PARSE]).split('\n')
        for entry in journal_authlog:
            try:
                if "authentication failure" in entry and "rhost= " not in entry:
                    offending_source = entry.split("rhost=")[1].split(" ")[0]
                    source_ip = convert_dns_to_ip(offending_source)

                    print("Appending %s to Baddies Bucket - we've never seen it before" % (source_ip))
                    baddies.append(source_ip)
            except:
                pass
                

            #Connection closed by 97.93.23.125 [preauth]
            try:
                if "Connection closed by " in entry and " [preauth]" in entry:
                    offending_source = entry.split("Connection closed by ")[1].split(" [preauth")[0]
                    source_ip = convert_dns_to_ip(offending_source)

                    print("Appending %s to Baddies Bucket - we've never seen it before" % (source_ip))
                    baddies.append(source_ip)
            except:
                pass

    except:
        print("journalctl auth.log not found.  Skipping.")
        pass

    try:
        for k, v in Counter(baddies).iteritems():
            if int(v) >= FAILED_AUTHS:
                # block with iptables locally
                block_bad_sources(k)

                # send blocked IPs to hive server
                payload = k
                if k not in BUSTED_THIS_SESSION:
                    BUSTED_THIS_SESSION.append(k)
                    call_home(payload)
                    print(k, v)
    except:
        pass


discover_bad_auths(BLOCK_AFTER_THIS_MANY, 10000000)
time.sleep(30)
block_baddies()
time.sleep(15)


while __name__ == "__main__":
    try:
        discover_bad_auths(BLOCK_AFTER_THIS_MANY, LINES_TO_PARSE)
        time.sleep(30)

    except KeyboardInterrupt:
        sys.exit()

